<?php

namespace App\Controller;

use App\Entity\Ingredient;
use App\Form\IngredientType;
use App\Repository\IngredientRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IngredientController extends AbstractController
{

    /**
     * DISPLAY ALL INGREDIENTS
     *
     * @param IngredientRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    #[Route('/ingredients', name: 'ingredients.index', methods: ['GET'])]
    public function index(IngredientRepository $repository, PaginatorInterface $paginator, Request $request): Response
    {

        $ingredients = $paginator->paginate(
            $repository->findBy(array(), array('createdAt' => 'DESC')),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('pages/ingredients/index.html.twig', [
            'ingredients' => $ingredients,
        ]);
    }









    /**
     * DISPLAY FORM TO CREATE AN INGREDIENT
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/ingredients/creation', name: 'ingredients.new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        EntityManagerInterface $manager
    ): Response {
        $ingredient = new Ingredient();
        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ingredient = $form->getData();

            $manager->persist($ingredient);
            $manager->flush();

            $this->addFlash(
                'success',
                'Votre ingrédient a été créé avec succès !'
            );

            return $this->redirectToRoute('ingredients.index');
        }

        return $this->render('pages/ingredients/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }








    #[Route('/ingredients/edit/{id}', name: 'ingredients.edit', methods: ['GET', 'POST'])]
    /**
     * DISPLAY FORM TO EDIT INGREDIENT
     *
     * @param Request $request
     * @param IngredientRepository $repository
     * @param Ingredient $ingredient
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(
        Request $request,
        IngredientRepository $repository,
        Ingredient $ingredient,
        EntityManagerInterface $manager
    ): Response {

        $form = $this->createForm(IngredientType::class, $ingredient);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ingredient = $form->getData();

            $manager->persist($ingredient);
            $manager->flush();

            $this->addFlash(
                'success',
                'Votre ingrédient a été modifié avec succès !'
            );

            return $this->redirectToRoute('ingredients.index');
        }


        return $this->render('pages/ingredients/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }



    #[Route('/ingredients/suppression/{id}', 'ingredients.delete', methods: ['GET'])]
    /**
     * DELETE INGREDIENT
     *
     * @param EntityManagerInterface $manager
     * @param Ingredient $ingredient
     * @return Response
     */
    public function delete(EntityManagerInterface $manager, Ingredient $ingredient): Response
    {

        $manager->remove($ingredient);
        $manager->flush();

        $this->addFlash(
            'success',
            'Votre ingrédient a été supprimmé avec succès'
        );

        return $this->redirectToRoute('ingredients.index');
    }
}
