<?php

namespace App\Controller;

use App\Entity\Recipe;
use App\Form\RecipeType;
use App\Repository\RecipeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RecipeController extends AbstractController
{
    /**
     * DISPLAY ALL RECIPES
     *
     * @param RecipeRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    #[Route('/recettes', name: 'recipes.index', methods: ['GET'])]
    public function index(
        RecipeRepository $repository, 
        PaginatorInterface $paginator, 
        Request $request
    ): Response {
        $recipes = $paginator->paginate(
            $repository->findBy(array(), array('createdAt' => 'DESC')),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('pages/recipes/index.html.twig', [
            'recipes' => $recipes,
        ]);
    }


    /**
     * 
     * DISPLAY FORM TO CREATE RECIPE
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/recettes/creation', name: 'recipes.new', methods: ['GET', 'POST'])]
    public function new(
        Request $request,
        EntityManagerInterface $manager
    ) : Response 
    {
        $recipe = new Recipe();
        $form = $this->createForm(RecipeType::class, $recipe);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $recipe = $form->getData();
            $manager->persist($recipe);
            $manager->flush();

            $this->addFlash(
                'success',
                'Votre recette a été créée avec succès !'
            );

            return $this->redirectToRoute('recipes.index');
        }

        return $this->render('pages/recipes/new.html.twig', [
            'form' => $form->createView()
        ]);
    }



    #[Route('/recettes/edit/{id}', name: 'recipes.edit', methods: ['GET', 'POST'])]
    /**
     * DISPLAY FORM TO EDIT A RECIPE
     *
     * @param Request $request
     * @param Recipe $recipe
     * @param EntityManagerInterface $manager
     * @return Response
     */
    public function edit(
        Request $request,
        Recipe $recipe,
        EntityManagerInterface $manager
    ): Response {

        $form = $this->createForm(RecipeType::class, $recipe);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $recipe = $form->getData();

            $manager->persist($recipe);
            $manager->flush();

            $this->addFlash(
                'success',
                'Votre recette a été modifiée avec succès !'
            );

            return $this->redirectToRoute('recipes.index');
        }


        return $this->render('pages/recipes/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }



    #[Route('/recettes/suppression/{id}', 'recipes.delete', methods: ['GET'])]
    /**
     * DELETE RECIPE
     *
     * @param EntityManagerInterface $manager
     * @param Recipe $recipe
     * @return Response
     */
    public function delete(
        EntityManagerInterface $manager, 
        Recipe $recipe
    ): Response{

        $manager->remove($recipe);
        $manager->flush();

        $this->addFlash(
            'success',
            'Votre recette a été supprimmée avec succès'
        );

        return $this->redirectToRoute('recipes.index');
    }
}
